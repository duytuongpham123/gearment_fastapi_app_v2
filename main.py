from typing import List

from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session
from typing import Optional

import database
from router.auth import api_auth_account
from router.api_file import file
from router.product import list_product, list_product_variant
import uvicorn


from pydantic import BaseModel

database.Base.metadata.create_all(bind=database.engine)

app = FastAPI()


app.include_router(api_auth_account.router)
app.include_router(file.router)
app.include_router(list_product.router)
app.include_router(list_product_variant.router)
#
if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
