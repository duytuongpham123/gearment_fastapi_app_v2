from schemas_data.product.list_product_shemas import ListProduct
from schemas_data.product.product_variant_schemas import ListProductVariant
from schemas_data.file.file_schemas import ListFile
from schemas_data.auth.auth import AuthAccount

RESPONSES_PRODUCT = {
    200: {
        "model": ListProduct,
        "description": "List Product",
        "content": {
            "application/json": {
                "example": {
                    "id": 1,
                    "title": "Heavy Cotton Women&#39;s Short Sleeve T-Shirt",
                    "brand": "Gildan",
                    "avatar": "https://gearment.com/wp-content/uploads/2022/07/G5000L_1-1-scaled.webp",
                    "description": "\n5.3 oz., pre-shrunk 100% cotton\nSeamless half-inch collar\nSide seamed\nCap sleeves\nDouble-needle stitched hems\nTaped neck and shoulders",
                    "variant_count": 185
                }
            }
        }
    }
}


RESPONSES_PRODUCT_VARIANT = {
    200: {
        "model": ListProductVariant,
        "description": "List File",
        "content": {
            "application/json": {
                "example": {
                    "product": {
                       "id": 1,
                       "title": "Heavy Cotton Women&#39;s Short Sleeve T-Shirt",
                       "brand": "Gildan",
                       "avatar": "https://gearment.com/wp-content/uploads/2022/07/G5000L_1-1-scaled.webp",
                       "variant_count": 1,
                       "description": "\n5.3 oz., pre-shrunk 100% cotton\nSeamless half-inch collar\nSide seamed\nCap sleeves\nDouble-needle stitched hems\nTaped neck and shoulders",
                       "variants": [
                          {
                             "id": 1137,
                             "product_id": 1,
                             "title": "LADIES T-SHIRT IRISH GREEN 2XL",
                             "size": "Irish Green",
                             "color": "2XL",
                             "color_code": "",
                             "price": 10.5
                          }
                       ]
                    }
                }
            }
        }
    },
    401: {
        "model": ListProductVariant,
        "description": "Problem Product Variant",
        "content": {
            "application/json": {
                "example": {
                    "Number between 1 and 10000": {
                       "code": 401,
                       "message": "[API] - Product_id must a number between 1 and 10000"
                    },
                    "Product not found": {
                       "code": 401,
                       "message": "[API] - Product_id must a number between 1 and 10000"
                    }
                }
            }
        }
    }
}


RESPONSES_AUTH = {
    200: {
        "model": AuthAccount,
        "description": "CREATE SUCCESS TOKEN",
        "content": {
            "application/json": {
                "example": {
                    "code": 200,
                    "message": "Authenticated",
                    "token": "YWJjZGVmZzE2NjY4NDI1NTguMDM4ODg0"
                }
            }
        }
    },
    401: {
        "model": AuthAccount,
        "description": "PROBLEM TOKEN",
        "content": {
            "application/json": {
                "example": {
                    "code": 401,
                    "message": "Server error",
                    "error": {
                        "reason": "BadRequest",
                        "message": "Sorry, your request could not be processed.",
                        "errorCode": "Unauthorized"
                    }
                }
            }
        }
    },
}


RESPONSES_FILE = {
    200: {
        "model": ListFile,
        "description": "List File",
        "content": {
            "application/json": {
                "example": {
                    "id": 3159,
                    "url": "gm-image/2022-09-07/92488bbd-b149-4a1e-81a6-fff6a7ab8857.png",
                    "filename": "dudieukien.png",
                    "mime_type": "png",
                    "created": 1662546019,
                    "thumbnail_url": "gm-image/2022-09-07/92488bbd-b149-4a1e-81a6-fff6a7ab8857.png"
                }
            }
        }
    }
}