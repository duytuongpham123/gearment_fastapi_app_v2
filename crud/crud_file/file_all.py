from sqlalchemy.orm import Session
from crud.crud_auth import auth


def get_all_file(db: Session, account_id):
    sql = """
            SELECT i.image_id,
                   i.image_type,
                   i.image_name,
                   i.is_review_image,
                   i.image_url,
                   i.image_path,
                   i.created_at,
                   al.sort_order
            FROM li.account_library al
                     LEFT JOIN li.image i ON i.image_id = al.image_id
            WHERE i.is_deleted = false
              AND al.is_deleted = false
                AND al.account_id = %s
        """ % account_id
    result = db.execute(sql)
    return result.all()


