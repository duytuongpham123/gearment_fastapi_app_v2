from sqlalchemy.orm import Session
from crud.crud_auth import auth


def get_all_product_variant(db: Session, product_id):
    sql_variant = """
            select 
                va.variant_id,
                va.blueprint_id,
                va.title,
                att1.attr_name,
                att2.attr_name,
                att3.attr_code,
                va.base_price
            from pr.variant va
            left join pr.attribute att1 on att1.attr_id = va.attr1
            left join pr.attribute att2 on att2.attr_id = va.attr2
            left join pr.attribute att3 on att3.attr_id = va.attr3
            where va.status = 1 and va.blueprint_id = %s
            group by 
                va.variant_id,
                va.blueprint_id,
                va.title,
                att1.attr_name,
                att2.attr_name,
                att3.attr_code,
                va.base_price
    """ % product_id
    sql_product = """
        select 
            pr_bl.blueprint_id,
            pr_bl.title,
            pr_bl.brand,
            img.image_url,
            pr_bl.description
        from pr.blueprint pr_bl
        join li.image_blueprint li_img on li_img.blueprint_id = pr_bl.blueprint_id
        join li.image img on li_img.image_id = img.image_id
        where pr_bl.blueprint_id = %s and li_img.is_thumbnail = true
        group by
            pr_bl.blueprint_id,
            pr_bl.title,
            pr_bl.brand,
            img.image_url,
            pr_bl.description
    """ % product_id
    result_product_variant = db.execute(sql_variant)
    result_product = db.execute(sql_product)
    try:
        product = result_product.one()
    except:
        product = []
    return product, result_product_variant.all()


