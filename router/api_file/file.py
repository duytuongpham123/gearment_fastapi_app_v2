from fastapi import FastAPI, Response, status, HTTPException, Depends, APIRouter
from sqlalchemy.orm import Session
from typing import List, Optional

from sqlalchemy import func
# from sqlalchemy.sql.functions import func
import model
from crud.crud_file import file_all
from crud.crud_auth import auth
from database import get_db
from schemas_data.file.file_schemas import ListFile
from fastapi.responses import JSONResponse
from responses_swagger import RESPONSES_FILE
# import request

router = APIRouter(
    prefix="/file",
    tags=['File']
)


@router.get("/", responses=RESPONSES_FILE)
def api_get_all_file(item: ListFile, db: Session = Depends(get_db)):
    flag = auth.check_token_account(db, item.token)
    if isinstance(flag, dict):
        return flag
    else:
        token = flag[2]
        res = file_all.get_all_file(db, token)
        arr_file = []
        for child in res:
            value = {
                'id': child.image_id,
                'url': child.image_path,
                'filename': child.image_name,
                'mime_type': child.image_type,
                'created': child.created_at,
                'thumbnail_url': child.image_path
            }
            arr_file.append(value)
        return {'code': 200, 'result': arr_file } if arr_file else JSONResponse(
                    status_code=204,
                    content={'code': 204, 'message': '[API] - Image not found.'}
                )