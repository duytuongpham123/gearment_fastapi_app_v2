from fastapi import FastAPI, Response, status, HTTPException, Depends, APIRouter
from sqlalchemy.orm import Session
from typing import List, Optional

from sqlalchemy import func
# from sqlalchemy.sql.functions import func
import model
from crud.crud_list_product import list_product
from database import get_db
from pydantic import BaseModel
from crud.crud_auth import auth
from schemas_data.product .list_product_shemas import ListProduct
from fastapi.responses import JSONResponse
from responses_swagger import RESPONSES_PRODUCT

router = APIRouter(
    prefix="/products",
    tags=['Products']
)




@router.get("/", responses=RESPONSES_PRODUCT)
def api_get_all_product(item: ListProduct, db: Session = Depends(get_db)):
    flag = auth.check_token_account(db, item.token)
    if isinstance(flag, dict):
        return flag
    else:
        res = list_product.get_all_product(db)
        all_product = []
        for child in res:
            value = {
                'id': child[0],
                'title': child[1],
                'brand': child[2],
                'avatar': child[4],
                'description': child[3],
                'variant_count': child[5]
            }
            all_product.append(value)
        new_arr = sorted(all_product, key=lambda x: x['id'])
        return {'code': 200, 'result': new_arr} if new_arr else JSONResponse(
            status_code=204,
            content={'code': 204, 'message': '[API] - Product not found.'}
        )