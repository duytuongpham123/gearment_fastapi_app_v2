from pydantic import BaseModel


class ListProductVariant(BaseModel):
   token: str
   id: int
