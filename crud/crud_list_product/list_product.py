from sqlalchemy.orm import Session


def get_all_product(db: Session):
        sql = """
                select blueprint_id, title, brand, description, img_url, count(variant_id) as variant 
                from
                    (
                        select 
                            pr_bl.blueprint_id as blueprint_id, 
                            pr_bl.title as title, 
                            pr_bl.brand as brand, 
                            pr_bl.description as description, 
                            img.image_url as img_url, 
                            va.variant_id as variant_id 
                        from pr.blueprint pr_bl
                        join li.image_blueprint li_img on li_img.blueprint_id = pr_bl.blueprint_id
                        join li.image img on li_img.image_id = img.image_id
                        join pr.variant va on va.blueprint_id = pr_bl.blueprint_id
                        where pr_bl.is_deleted = false 
                         and pr_bl.status = 1 
                         and va.status = 1
                         and li_img.is_thumbnail = true
                        group by pr_bl.blueprint_id, pr_bl.title, pr_bl.brand, pr_bl.description, img.image_url, va.variant_id
                    ) foo
                group by blueprint_id, title, brand, img_url, description
            """
        result = db.execute(sql)
        return result.all()


