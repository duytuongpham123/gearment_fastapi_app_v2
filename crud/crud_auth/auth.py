from sqlalchemy.orm import Session
from datetime import datetime, timedelta

def get_auth_account(db: Session, api_key, api_secret):
    sql = """
        select * from pe.account_api_token where api_key = '%s' and api_secret = '%s'
    """ % (api_key, api_secret)
    result = db.execute(sql)
    return result.all()


def check_token_account(db: Session, param_token):
    if not param_token:
        return {
            "code": 401,
            "message": "[API] Unknown request"
        }
    sql = """
        select * from pe.account_api_accesstoken where access_token = '%s'
    """ % param_token
    result = db.execute(sql)
    try:
        res = result.one()
    except:
        res = []
    if not res:
        return {
            "code": 401,
            "message": "[API] - Unauthorized"
        }
    else:
        if res[3] == int(datetime.utcnow().timestamp()):
            return {
                "code": 401,
                "message": "[API] - Invalid Token"
            }
    return res


