from fastapi import FastAPI, Response, status, HTTPException, Depends, APIRouter
from sqlalchemy.orm import Session
from typing import List, Optional

from sqlalchemy import func
# from sqlalchemy.sql.functions import func
import model, schemas
from crud.order import order
from database import get_db

router = APIRouter(
    prefix="/test",
    tags=['Test']
)

# response_model 
# Trả giá trị được config trong schemas chứa Response
@router.get("/", response_model=List[schemas.Order])
def test(db: Session = Depends(get_db)):
    res = order.get_nh_order(db)
    print(999999, res)
    return [{
        'id': 1,
        'name': 'Tường đẹp trai'
    }]
