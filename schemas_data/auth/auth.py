from typing import List, Union

from pydantic import BaseModel


class AuthAccount(BaseModel):
   api_key: str
   api_secret: str
