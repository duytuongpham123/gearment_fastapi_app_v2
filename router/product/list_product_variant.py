from fastapi import FastAPI, Response, status, HTTPException, Depends, APIRouter
from sqlalchemy.orm import Session
from typing import List, Optional

from sqlalchemy import func
# from sqlalchemy.sql.functions import func
import model
from crud.crud_list_product import detail_product_variant
from database import get_db
from pydantic import BaseModel
from crud.crud_auth import auth
from schemas_data.product.product_variant_schemas import ListProductVariant
from fastapi.responses import JSONResponse
from responses_swagger import RESPONSES_PRODUCT_VARIANT

router = APIRouter(
    prefix="/products/variant",
    tags=['Products Variant']
)


@router.get("/", responses=RESPONSES_PRODUCT_VARIANT)
def api_get_all_product_variant(item: ListProductVariant, db: Session = Depends(get_db)):
    flag = auth.check_token_account(db, item.token)
    if isinstance(flag, dict):
        return flag
    else:
        res = detail_product_variant.get_all_product_variant(db, item.id)
        status_code = {'code': 401}
        if not res[0]:
            status_code.update({'message': '[API] - Product not found'})
        if item.id < 1 or item.id > 10000:
            status_code.update({'message': '[API] - Product_id must a number between 1 and 10000'})
        if len(status_code) == 2:
            return JSONResponse(
                status_code=401,
                content=status_code
            )
        all_variant = []
        obj_product = {
          "id": res[0][0],
          "title": res[0][1],
          "brand": res[0][2],
          "avatar": res[0][3],
          "variant_count": len(res[1]),
          "description": res[0][4]
        }
        for child in res[1]:
            value = {
                'id': child[0],
                'product_id': child[1],
                'title': child[2],
                'size': child[3] or '',
                'color': child[4] or '',
                'color_code': child[5] or '',
                'price': child[6]
            }
            all_variant.append(value)
        obj_product.update({'variants': all_variant})
        result = {
            'code': 200,
            'product': obj_product,
        }
        return result

