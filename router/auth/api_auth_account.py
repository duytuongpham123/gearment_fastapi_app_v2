from fastapi import FastAPI, Response, status, HTTPException, Depends, APIRouter
from sqlalchemy.orm import Session
from typing import List, Optional

from sqlalchemy import func
# from sqlalchemy.sql.functions import func
import model
from crud.crud_auth import auth
from database import get_db, engine
from pydantic import BaseModel
# import request
from datetime import datetime, timedelta
import base64
from schemas_data.auth.auth import AuthAccount
from fastapi.responses import JSONResponse
from responses_swagger import RESPONSES_AUTH

router = APIRouter(
    prefix="/auth/get_token",
    tags=['Auth']
)


@router.post("/", responses=RESPONSES_AUTH)
def api_auth_account(item: AuthAccount, db: Session = Depends(get_db)):
    res = auth.get_auth_account(db, item.api_key, item.api_secret)
    if not res:
        return JSONResponse(status_code=401, content={
            "code": 401,
            "message": "Server error",
            "error": {
              "reason": "BadRequest",
              "message": "Sorry, your request could not be processed.",
              "errorCode": "Unauthorized"
            }
        })
    else:
        delete_token = """
            DELETE FROM pe.account_api_accesstoken
            WHERE account_id = %s;
        """ % int(res[0][1])
        engine.execute(delete_token)
        # create token
        gen_token = '%s%s%s' % (item.api_key, item.api_secret, datetime.utcnow().timestamp())
        current_create_at = datetime.utcnow().timestamp()
        current_token_expired = (datetime.today() + timedelta(days=30)).timestamp()
        create_token = """
            INSERT INTO
                pe.account_api_accesstoken (
                    access_token,
                    account_id,
                    token_expired_at,
                    created_at
                )
            VALUES
                ('%s', %s, %s, %s)
        """ % (base64.b64encode(gen_token.encode()).decode(), int(res[0][1]), int(current_token_expired), int(current_create_at))
        engine.execute(create_token)
        return {
            "code": 200,
            "message": "Authenticated",
            "token": base64.b64encode(gen_token.encode()).decode()

        }
